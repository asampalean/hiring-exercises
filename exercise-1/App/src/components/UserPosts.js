import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import CardLayout from './CardLayout'

const GET_POST = gql`
  {
    user(id: 1) {
        name
    posts {
      data {
        id
        title
      }
    }
    }
  }
`;

const UserPosts = () => {
  const { loading, error, data } = useQuery(GET_POST);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  return(
      <CardLayout>
          <>     
            <div className="text-gray-900 font-bold text-lg mb-6">{data.user.name}'s posts</div>
            <div className="text-gray-900 text-md mb-6">
            
                
            
                {data.user.posts.data.map(posts => (
                  <li className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
                    Title: {posts.title}
                  </li>
                ))}
          

            </div>
            
          </>
      </CardLayout>
    
  )
}


export default UserPosts