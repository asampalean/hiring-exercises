import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import CardLayout from './CardLayout'

const GET_USER = gql`
  {
    user(id: 1) {
      id
      name
      email
      address {
        street
        suite
        city
      }
      phone
      company {
        name
      }
    }
  }
`;

const UserInfo = () => {
  const { loading, error, data } = useQuery(GET_USER);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  return(
      <CardLayout>
          <>     
            <div className="text-gray-900 font-bold text-3xl mb-6">Hello, {data.user.name} </div>
            <div className="mx-auto lg:mx-0 mb-2 border-b-2 border-teal-500 opacity-25"></div>
        <div className="text-gray-900 text-md mb-6">
          <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">Address :</p> 
          <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.address.street}, {data.user.address.suite}, {data.user.address.city}</p>
      
        </div>
        <div className="text-gray-900 text-md mb-6">
         <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">Email:</p> 
         <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.email}</p>
        </div>
        <div className="text-gray-900 text-md mb-6">
         <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">Phone: </p> 
         <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.phone}</p> 
        </div>
        <div className="text-gray-900 text-md mb-6">
          <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">Company:</p> 
          <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">{data.user.company.name}</p>
        </div>
          </>
      </CardLayout>
    
  )
}


export default UserInfo