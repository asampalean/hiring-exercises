import React from 'react';

const CardLayout = ({children}) => {
  return(
    <div className="max-w-sm w-full lg:max-w-full lg:flex mt-12">
      <div className="bg-white flex flex-col justify-between leading-normal m-auto p-12 text-center lg:text-left rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl opacity-75">
        {children}
      </div>
    </div>  
  )
}

export default CardLayout