import React from 'react';
import logo from './logo.svg';
import UserInfo from './components/UserInfo';
import UserPosts from './components/UserPosts';
import { ApolloProvider } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
  uri: 'https://graphqlzero.almansi.me/api',
  cache: new InMemoryCache()
});


function App() {
  return (
    <div className="App bg-gray-300 bg-cover w-full h-screen mt-0 p-12"
    style={{
      backgroundImage: "url('https://source.unsplash.com/1L71sPT5XKc')",
    }}
    >
        <div className="max-w-sm w-full lg:max-w-full lg:flex">
        
  
      <ApolloProvider client={client}>
        <UserInfo />
        <UserPosts/>
      </ApolloProvider>  
      </div>
  </div>
  );
}

export default App;
