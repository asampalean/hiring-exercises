const fs = require('fs')
const Stripe = require('stripe');
const STRIPE_TEST_SECRET_KEY = 'rk_test_51EDa6GIrkMUcWfFwXon9vsJYpTqX2eTqbINAUf4fZC7ivToWv59cAPoHdYhmszwL9ZKWJtUUbaCcHtpjl6rWlXLP00C1dcAoUR'
const stripe = Stripe(STRIPE_TEST_SECRET_KEY);

const handler = async (country) => {

  try{
    let finalCustomers = []

    /* add code below this line */

    // 1. reading data and storing it in variables

    const customers = fs.readFileSync('customers.json', 
    {encoding:'utf8', flag:'r'}); 
    const countries = fs.readFileSync('countries-ISO3166.json', 
    {encoding:'utf8', flag:'r'}); 
    let rawCustomers = JSON.parse(customers)
    let rawCountries = JSON.parse(countries)

    // 2. filter the customers by country

    let filteredCountryCustomers = rawCustomers.filter(customer => customer.country === country)
   
     // 3. getting the country code

    let countryCode = Object.keys(rawCountries).filter(function(key){
      return rawCountries[key] === country
    });
  
      // 4. transform customers to save into Stripe
   
      let customersData = filteredCountryCustomers.map(cust => {
         const container = {
           email: cust.email,
            name: cust.first_name, 
            firstLine: cust.address_line_1 
         }
         return container
      })

    // 5. for each customer create a Stripe customer
 
  const stripeCustomer =  () => {
   
     customersData.forEach(async (element) => {
      const customer = await stripe.customers.create({
        email: element.email,
        name:  element.name,
        address:{
          line1: element.firstLine,
          country: countryCode.toString()
        }
      });
      console.log(customer)

  // 6. push into finalCustomers the stripe customers with email, country and id as properties.

    finalCustomers.push({
      email: customer.email,
      customerID: customer.id,
      country: customer.address.country
    })

   // 7. write finalCustomers array into final-customers.json using fs

    let data = JSON.stringify(finalCustomers)
    fs.writeFileSync('final-customers.json', data, err => {
      if (err) {
        console.error(err)
        return
      }
     
    })
    console.log(finalCustomers)
    })
  }

  stripeCustomer()

    /* 
      finalCustomers array should look like:
      finalCustomers = [{
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        },
        {
          email: test@test.com
          customerId: 1d833d-12390sa-9asd0a2-asdas,
          country: 'ES'
        }
      }] 
    */

    /* add code above this line */

}catch(e){
  throw e
}
 
} 

handler('Spain')
